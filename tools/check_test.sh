#!/bin/bash

ELF="$1"
EXPECT="$2"
ASSERT_VAR=${0%/*}/assert_var.py

if [ ! -f "$ELF" ]; then
	echo "ELF file '$ELF' not found"
	exit 1
fi

if [ ! -f "$EXPECT" ]; then
	echo ".expect file '$EXPECT' not found"
	exit 1
fi

function get_varname {
	echo $1
}

EXIT_CODE=0

while read -r line
do
	VARNAME=$(get_varname $line)
	echo -n "$VARNAME: "

	OUT="$($ASSERT_VAR $ELF $line)"
	if [ $? -eq 0 ]
	then
		echo -e "\e[32mPASSED"
	else
		echo -e "\e[31mFAILED"
		EXIT_CODE=1
	fi

	# reset formatting
	echo -e -n  "\e[0m"

	if [ -n "$OUT" ]; then
		echo "$OUT"
	fi

done < "$EXPECT"

exit $EXIT_CODE
