Frequently Asked Questions about ezconsole
==========================================

# Purpose: what is this tool for

This tool acts as a serial console for the ez430 firmware. It connects
to the ez430U, the USB programming dongle of the ez430-RF2500, and all
incomming characters are printed  on stdout. Conversely, any character
input on the terminal will be sent over USB-serial.

TODO: add a word about cdc_acm, why it once was unsuited, etc.

# Access permissions

Just like  `mspdebug`, `ezconsole`  requires read/write access  to the
USB port.  Either launch it via  `sudo`, or make sure  that the proper
`udev`  rules  are installed.  (typically  installing  mspdebug via  a
package manager will also install the right `udev` rules)

cf http://dlbeer.co.nz/mspdebug/faq.html#usb_noroot

# How to compile and build

just type  `make` and everything should  work automatically. Otherwise
you're on your own.

In  order for  the binary  to  be as  portable as  possible, we  build
`ezconsole` with all libraries linked statically.

Supported platforms are Linux x86 (32bits) and x64, and also Raspberry
Pi  (ARM 32  bits). All  third-party libraries  required are  included
under the  `3rd/lib` directory,  organized by  architecture. (original
packages are included in the `3rd/deb` directory)

Note  that OSX  is  not supported,  as we  never  got libusb  correcly
working on this platform.

# The serial port on the ez430 is buggy !

short answer: yes, it's not-suitable-for-production (tm). use a proper
USB/serial converter instead.

TODO: document  what happens when the  serial buffer on the  3410 gets
full and  the serial port  stops working. found  no solution to  it in
software, but maybe at least we could detect the condition from within
ezconsole and print an explicit message.

# Troubleshooting

hint: recompile with DEBUG=1  and/or run with `export LIBUSB_DEBUG=4`
so that you get verbose information about what is happening.
