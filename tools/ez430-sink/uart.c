// GS-2016-07-21-16:27 copy-pasting the original ez430  BSP

#include <msp430.h>

#include <string.h>
#include <stdio.h>

#include "uart.h"

// text string
void uart_print(char* s)
{
    int i;
    for(i=0; s[i] ; i++)
    {
        uart_putchar(s[i]);
    }
}

uart_putchar(int c)
{
    while (!(IFG2&UCA0TXIFG));              // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;
    return (unsigned char)c;
}

int uart_getchar(void)
{
    int c;
    c = UCA0RXBUF;
    return c;
}

/*********************** copy-paste starts here ************************/


/*
 * USCI doc:
 *   UART user manual page 461
 *   SPI  user manual page 497
 *
 * USCI_A : serial
 * USCI_B : SPI
 *
 * Port 3
 * ======
 *   3.7 :
 *   3.6 :
 *   3.5 : uart data in    -> battery expansion board
 *   3.4 : uart data out   -> battery expansion board
 *   3.3 : spi clock (timer + flash + radio)
 *   3.2 : spi somi  (timer + flash + radio)
 *   3.1 : spi simo  (timer + flash + radio)
 *   3.0 : radio CSN (~CS)
 */

#define BIT_RX (1<<5)
#define BIT_TX (1<<4)

static volatile uart_cb_t uart_cb;


void uart_init(int config)
{
    P3SEL |=  (BIT_TX | BIT_RX); /* uart   */
    P3DIR |=  (BIT_TX         ); /* output */
    P3DIR &= ~(         BIT_RX); /* input  */

    switch (config) {
        case UART_9600_SMCLK_1MHZ:
            UCA0CTL1 = UCSSEL_2;                      // SMCLK
            UCA0BR0 = 0x68;                           // 9600 from 1Mhz
            UCA0BR1 = 0x00;
            UCA0MCTL = UCBRS_2;
            UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
            break;

        case UART_9600_SMCLK_8MHZ:
            UCA0CTL1 = UCSSEL_2;                      // SMCLK
            UCA0BR0 = 0x41;                           // 9600 from 8Mhz
            UCA0BR1 = 0x03;
            UCA0MCTL = UCBRS_2;
            UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
            break;

        default:
            break;
    }

    uart_cb = NULL;
}
