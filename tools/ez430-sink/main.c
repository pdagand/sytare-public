#include <msp430.h>
#include <stdint.h>


#include "button.h"
#include "cc2500.h"
#include "clock.h"
#include "leds.h"
#include "spi.h"
#include "timer.h"
#include "uart.h"

// maximum payload size, excluding length byte and checksum
// (64 is the largest size supported by the driver)
#define MAX_MSG_SIZE 64

static uint8_t buffer_rx_msg [MAX_MSG_SIZE];
static int     buffer_rx_rssi;

// zero == no packet to process
// non-zero == indicates lentgh of received packet
//
// set by the callback then packer received
// read by main loop, and set to zero after when packet is processed
static int     buffer_rx_length;

// Interrupt callbacks (invoked from ISR context)

void radio_callback(uint8_t* buffer, int size, int8_t rssi);
void button_callback(void)
{
    WDTCTL=0; //dummy write to watchdog will trigger a reboot
}

char *hex_digits="0123456789ABCDEF";

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    // platform initialization
    set_mcu_speed_dco_mclk_16MHz_smclk_8MHz();
    uart_init(UART_9600_SMCLK_8MHZ);
    leds_init();
    spi_init();
    cc2500_init();
    timerA_init();
    button_init();

    button_register_cb(button_callback);
    button_enable_interrupt();


    // configure a timer to wake us up once in a while, so that we can
    // put the radio in 'receive' state again. (this is useful because
    // the radio may end up in an error state sometimes)
    timerA_set_wakeup(1);
    timerA_start_milliseconds(1100);

    buffer_rx_length = 0;

    // register a callback to be called upon packet reception
    cc2500_rx_register_buffer(buffer_rx_msg, MAX_MSG_SIZE);
    cc2500_rx_register_cb(radio_callback);

    // last but not least, listing for packets
    cc2500_rx_enter();

    uart_print("==== ez430 Sytare Sink ====\n");
    __enable_interrupt();
    for(;;)
    {
        LPM1;
        led_red_switch();

        if (buffer_rx_length)
        {// callback says we have a packet to process. let's do it
        
            uart_print("RX:");
            
            int i;
            for (i=0; i< buffer_rx_length ; i++)
            {
                
                uint8_t byte=buffer_rx_msg[i];

                uart_putchar(' ');
                uart_putchar( hex_digits[byte/16] );
                uart_putchar( hex_digits[byte%16] );
                 
            }
            uart_print("\n");
            
            buffer_rx_length = 0;
            timerA_start_milliseconds(1100);
            // GS-2016-07-21 do we need the RSSI stuff ? don't think so.
        }
        else
        {// we were either woken by the timer, or by an incorrect reception

            // in any case, let's put the radio back to work
            cc2500_idle();
            cc2500_rx_enter();
            timerA_start_milliseconds(1100);
        }
    }
}


// GS-2016-07-21  this  callback is  taken  almost  verbatim from  the
// original ez430 BSP, in drivers/examples/radio/
void radio_callback(uint8_t* buffer, int size, int8_t rssi)
{
    led_green_switch();
    switch (size)
    {
        case 0:
            // GS-2016-07-21 I  believe this  should never  happen but
            // I'm leaving it here just in case :-)
            uart_print("RX: ERROR ZEROSIZE\n");
            break;
        case -EEMPTY:
            uart_print("RX: ERROR EMPTY\n");
            break;
        case -ERXFLOW:
            uart_print("RX: ERROR RXOVERFLOW\n");
            break;
        case -ERXBADCRC:
            uart_print("RX: ERROR BADCRC\n");
            break;
        default:
            if (size > 0)
            {
                buffer_rx_rssi = rssi;
                buffer_rx_length = size;
            }
            else
            {
                uart_print("RX: ERROR UNKNOWN");
            }
            break;
    }
    cc2500_idle();
    cc2500_rx_enter();
    led_green_switch();
}
