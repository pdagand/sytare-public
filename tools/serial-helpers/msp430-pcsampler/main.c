#include <msp430.h>
#include <stdint.h>

void uart_putc(char c)
{
    while (!(UCA0IFG & UCTXIFG));
    UCA0TXBUF = c;
}

__attribute__((interrupt(TIMER0_A0_VECTOR)))
void tim0_isr(void)
{
    // make sure this is allocated on the stack
    volatile uint16_t first;

    // 1 pushed registers, variable 'first' and sr pushed by IRQ before pc on
    // stack => 3 registers to skip
    // NOTE: you have to check disassembly!
    const uint8_t* pc = (uint8_t*)(&first + 3);

    // CCIFG is self-clearing since this is its exclusive ISR

    P3OUT ^= BIT5;

    uart_putc(pc[1]);
    uart_putc(pc[0]);
}

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;


    /* Clock setup */

    // Unlock clock system
    CSCTL0_H = (0xA5);

    // set DCO speed to 16MHz
    uint16_t reg_clk = CSCTL1;
    reg_clk &= ~( DCOFSEL0 | DCOFSEL1);
    reg_clk |= (DCORSEL | DCOFSEL_2);
    CSCTL1 = reg_clk;

    // set MCLK div to 1 => 16 MHz
    reg_clk = CSCTL3;
    reg_clk &= ~(DIVM0 | DIVM1 | DIVM2);
    reg_clk |= DIVM__1;

    // set SMCLK div to 4 => 4 MHz
    reg_clk &= ~(DIVS0 | DIVS1 | DIVS2);
    reg_clk |= DIVS__4;

    CSCTL3 = reg_clk;

    // lock back clock system
    CSCTL0_H = 0;


    /* UART setup */

    UCA0CTLW0 |= UCSWRST_L;

    // select clock source: SMCLK
    UCA0CTLW0 |= UCSSEL_2;

    // 115200 baud for 4 MHz BRCLK (=SMCLK)
    UCA0BRW = 2;
    UCA0MCTLW = UCOS16 | (2 << 4) | (0xbb << 8);

    // enable UART
    UCA0CTLW0 &= ~(UCSWRST_L);

    // configure UART TX pin
    P2SEL1 |= BIT0;

    // output header bytes
    uart_putc(0x12);
    uart_putc(0x34);
    uart_putc(0x56);
    uart_putc(0x78);


    /* Timer setup */

    // frequency: 4 MHz / 8 = 500kHz
    TA0CTL = TASSEL_2   | // SMCLK
             ID__8      | // div 8
             MC__UP     | // Up-Mode
             TACLR;       // clear to zero

    // compare interrupt @ 100 Hz => every 100ms
    TA0CCR0 = 50000;

    // enable compare interrupt
    TA0CCTL0 = CCIE;

    __enable_interrupt();


    // initialize blinking of LEDs
    P3DIR |= BIT4 | BIT5;


    while(1) {
        // do something
        P3OUT ^= BIT4;
        __delay_cycles(16000000 / 10);
        // do something
        P3OUT ^= BIT4;
        __delay_cycles(16000000 / 10);
        // do something
        P3OUT ^= BIT4;
        __delay_cycles(16000000 / 10);
    }
}
