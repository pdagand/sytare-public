#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <libserialport.h>

size_t bytes_recv = 0;
size_t lines = 0;

void print_header(int lpad)
{
	for(int i = 0; i < lpad; i++) {
		printf(" ");
	}

	for(char i = 0; i <= 0xf; i++) {
		printf("%2x ", i);
	}

	printf("\n");
}

int main(int argc, const char* argv[])
{
	struct sp_port* port;
	if(sp_get_port_by_name(argv[1], &port) != SP_OK) {
		printf("Cannot open port %s\n", argv[1]);
		return 1;
	}

	sp_open(port, SP_MODE_READ);
	sp_set_baudrate(port, 115200);
	sp_set_xon_xoff(port, SP_XONXOFF_DISABLED);


	bool new_line = true;

	while(1) {

		if(new_line) {
			printf("\n");
			new_line = false;

			char buf[32];
			const size_t curr_addr = bytes_recv & ~(0xf);
			const int chars = snprintf(buf, sizeof(buf), "0x%02x: ", curr_addr);

			if(lines % 10 == 0) {
				print_header(chars);
			}

			printf("%s", buf);

			lines++;
		}

		uint8_t c;
		int ret = 0;
		while(ret <= 0) {
			ret = sp_blocking_read(port, &c, sizeof(c), 0);
		}
		bytes_recv++;

		if(c == 0x42)
			printf("\033[31m");

		printf("%02x ", c);

		if(c == 0x42)
			printf("\033[0m");

		fflush(0);

		if(bytes_recv % 16 == 0) {
			new_line = true;
		}
	}
}
