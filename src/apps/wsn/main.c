/*
 * \file   wsn_demo.c
 * \brief  Measures ambient temperature and report via radio
 * \author Tristan Delizy
 * \author Daniel Krebs
 *
 * This application is a demonstration for the SYTARE project
 * compiling this along with the SYTARE project will allow to
 * run this application on TI FR5739 board under intermittent
 * power supply conditions.
 */

#include <msp430.h>
#include <stdint.h>

#include <kernel.h>

#include <drivers/spi.h>
#include <drivers/cc2500.h>
#include <drivers/utils.h>
#include <drivers/led.h>
#include <drivers/temperature.h>

#define AVG_MEASURE_COUNT   (10)
#define DELAY_MS(n) __delay_cycles(24000000 / 1000 * n)

void check_radio_error(unsigned int error)
{
    leds_off();

    switch(error) {
        case EEMPTY_RF:     led_on(1); break;
        case ERXFLOW_RF:    led_on(2); break;
        case ERXBADCRC_RF:  led_on(3); break;
        case ETXFLOW_RF:    led_on(4); break;
        case ESPI_RF:       led_on(5); break;
        case EBADCHIP_RF:   led_on(6); break;
        case EBADCONF_RF:   led_on(7); break;
        case EBADSTATE_RF:  led_on(8); break;
        default:            return;
    }

    while(1);
}

int sensor_loop(void)
{
    uint16_t msg_count = 0;

    while(msg_count < 50) {

        int16_t sample = 0;
        for(int i = 0; i < AVG_MEASURE_COUNT; i++) {

            sample += syt_tmp_drv_sample();

            // simulate some heavy calculations
            DELAY_MS(1);
        }

        struct packet_t {
            uint8_t id;
            uint8_t temperature;
            uint8_t data[2];
        };

        struct packet_t pkt;
        pkt.id = msg_count;
        pkt.temperature =  sample / AVG_MEASURE_COUNT;

        syt_cc2500_wakeup();
        check_radio_error(syt_cc2500_send_packet((char*) &pkt, sizeof(pkt)));
        syt_cc2500_sleep();

        msg_count++;

        DELAY_MS(5);
    }

    return 0;
}

int main(void)
{
    syt_prt_drv_init();
    syt_tmp_drv_init();

    leds_init();

    // initialize SPI
    syt_spi_init();
    syt_spi_configure(&spiSettings_default_config);

    // initialize radio
    check_radio_error(syt_cc2500_init());
    syt_cc2500_configure(&rfSettings_default_config, 1);

    syt_cc2500_idle();
    syt_cc2500_sleep();

    return sensor_loop();
}
