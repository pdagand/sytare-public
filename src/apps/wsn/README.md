# WSN app

For benchmarking with ez430-sink, checkout the branch `tools/ez430-sink-wsn`
and reflash the ez430-sink:

```bash
$ git checkout tools/ez430-sink-wsn
$ cd tools/ez430-sink
$ make run
```

Make sure you are flashing the right board if you also have a MSP430FR5739
connected at the same time.
