/**
 *  \file   clock.c
 *  \brief  TI FR5739 board lib, clock system
 *  \author Tristan Delizy
 *  \date   2016
 **/

/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/utils.h>
#include <drivers/clock.h>
#include <drivers/port.h>
#include <drivers/dma.h>


/******************************************************************/
/********************************************** INTERNAL DEFINES **/

#define CLK_SYS_PSWD            (0xA5)


/******************************************************************/
/******************************************* INTERNALS FUNCTIONS **/

static void unlock_clock_system(void);
static void lock_clock_system(void);
static void delay_4_cycles(register unsigned int n);
static void apply_config(void);
unsigned int clk_set_dco_freq(unsigned long dco_freq);


/******************************************************************/
/********************************************************* TYPES **/

// msp430 clock descriptor
struct syt_clock_desc_t
{
    clock_src_t src;
    clock_div_t div;
};

// sytare clock driver data descriptor for persistency
struct clk_device_context_t
{
    unsigned long               dco_freq;
    struct syt_clock_desc_t     mclk;
    struct syt_clock_desc_t     smclk;
    struct syt_clock_desc_t     amclk;
};

/******************************************************************/
/****************************************** MODULE CONFIGURATION **/

static
const struct clk_device_context_t _clk_default = {
    .dco_freq   = CLK_DCO_24_MHZ,
    .mclk       = { CLK_DCO, CLK_DIV_8 },
    .smclk      = { CLK_DCO, CLK_DIV_8 },
    .amclk      = { CLK_DCO, CLK_DIV_1 }
};

/******************************************************************/
/***************************************************** VARIABLES **/

static
syt_dev_ctx_changes_t ctx_changes;

// local copy in RAM
static
struct clk_device_context_t clk_device_context;


/******************************************************************/
/***************************** SYTARE PERSISTANCY IMPLEMENTATION **/

void clk_drv_init(void)
{
    syt_drv_register(clk_drv_save, clk_drv_restore,
                    sizeof(struct clk_device_context_t));

    // initializa driver data structure
    clk_device_context = _clk_default;

    apply_config();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return;
}


void clk_drv_save(int drv_handle)
{
    drv_save(&ctx_changes,
             syt_drv_get_ctx_next(drv_handle),
             &clk_device_context,
             sizeof(clk_device_context));
}


void clk_drv_restore(int drv_handle)
{
    dma_memcpy(&clk_device_context,
               syt_drv_get_ctx_last(drv_handle),
               sizeof(clk_device_context));

    // restore the hardware state
    apply_config();

    // mark as dirty to make sure that it will be saved to the new checkpoint
    // that is still uninitialized after driver restoration
    drv_mark_dirty(&ctx_changes);
}


/******************************************************************/
/******************************************* LOW LEVEL FUNCTIONS **/

static void unlock_clock_system(void)
{
    CSCTL0_H = CLK_SYS_PSWD;
}

static void lock_clock_system(void)
{
    CSCTL0_H = 0;
}

static void delay_4_cycles(register unsigned int n)
{
    __asm__ __volatile__ (  "1:             \n"
                            " dec   %[n]    \n"
                            " nop           \n"
                            " jne   1b      \n"
                            : [n] "+r"(n)
                            );
}

static void apply_config(void)
{
    clk_set_dco_freq(clk_device_context.dco_freq);

    clk_set_mclk_src(clk_device_context.mclk.src);
    clk_set_smclk_src(clk_device_context.smclk.src);
    clk_set_aclk_src(clk_device_context.amclk.src);

    clk_set_mclk_div(clk_device_context.mclk.div);
    clk_set_smclk_div(clk_device_context.smclk.div);
    clk_set_aclk_div(clk_device_context.amclk.div);
}

unsigned long clk_get_dco_freq()
{
    return clk_device_context.dco_freq; // /!\ for now storing dco freq in all clock frequency as they can't have other variable sources
}

unsigned int clk_set_dco_freq(unsigned long dco_freq)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(dco_freq){
        case CLK_DCO_5_33_MHZ : temp = CSCTL1;
                                temp &= ~(DCORSEL | DCOFSEL0 | DCOFSEL1);
                                temp |= DCOFSEL_0;
                                CSCTL1 = temp;
                                break;
        case CLK_DCO_6_67_MHZ : temp = CSCTL1;
                                temp &= ~(DCORSEL | DCOFSEL0 | DCOFSEL1);
                                temp |= DCOFSEL_1;
                                CSCTL1 = temp;
                                break;
        case CLK_DCO_8_MHZ :    temp = CSCTL1;
                                temp &= ~(DCORSEL | DCOFSEL0 | DCOFSEL1);
                                temp |= DCOFSEL_3;
                                CSCTL1 = temp;
                                break;
        case CLK_DCO_16_MHZ :   temp = CSCTL1;
                                temp &= ~( DCOFSEL0 | DCOFSEL1);
                                temp |= (DCORSEL | DCOFSEL_0);
                                CSCTL1 = temp;
                                break;
        case CLK_DCO_20_MHZ :   temp = CSCTL1;
                                temp &= ~( DCOFSEL0 | DCOFSEL1);
                                temp |= (DCORSEL | DCOFSEL_1);
                                CSCTL1 = temp;
                                break;
        case CLK_DCO_24_MHZ :   temp = CSCTL1;
                                temp &= ~( DCOFSEL0 | DCOFSEL1);
                                temp |= (DCORSEL | DCOFSEL_3);
                                CSCTL1 = temp;
                                break;
        default :               ret = EBADRATE_CLK;
                                break;
    }

    // set the driver data value accordingly
    if(ret == ESUCCESS_CLK)
        clk_device_context.dco_freq = dco_freq;

    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}


/******************************************************************/
/********************************************************* UTILS **/

void clk_delay_micro(unsigned int delay)
{
    // compiler optimizes this to only one division
    const int ticks_per_loop = 4;
    const unsigned long us_per_s = 1000000;

    uint32_t loops = 0;

    if(clk_device_context.mclk.src == CLK_DCO) {
        loops = (delay * clk_device_context.dco_freq) / clk_device_context.mclk.div / ticks_per_loop / us_per_s;
    } else {
        loops = (delay * CLK_VLO_NOMINAL_FREQ) / clk_device_context.mclk.div / ticks_per_loop / us_per_s;
    }

    if(loops == 0)
        return;

    __asm__ __volatile__ (  "1:             \n"
                            " dec   %[loops]\n"
                            " nop           \n"
                            " jne   1b      \n"
                            : [loops] "+r"(loops)
                            );
}

void clk_delay_ms(unsigned int delay)
{
    clk_delay_micro(delay * 1000);
}

// light up leds (without pin initialisation) wait 3 seconds and turn them off
// usefull function for debugging syscalls
unsigned int clk_dummy_call(unsigned int test1, unsigned int test2, unsigned int test3, unsigned int test4)
{
    prt_out_bis(5, (BIT0 | BIT1 | BIT2 | BIT3));
    prt_out_bis(3, (BIT4 | BIT5 | BIT6 | BIT7));
    clk_delay_ms(2000);
    prt_out_bic(5, (BIT0 | BIT1 | BIT2 | BIT3));
    prt_out_bic(3, (BIT4 | BIT5 | BIT6 | BIT7));
    drv_mark_dirty(&ctx_changes);
    return (test1 + test2 + test3 + test4);
}

/******************************************************************/
/********************************************** SUB MASTER CLOCK **/

unsigned long clk_get_smclk_speed()
{
    if(clk_device_context.smclk.src == CLK_DCO)
        return clk_device_context.dco_freq / ((unsigned long)clk_device_context.smclk.div);
    else
        return CLK_VLO_NOMINAL_FREQ / ((unsigned long)clk_device_context.smclk.div);
}

clock_src_t  clk_get_smclk_src()
{
    return clk_device_context.smclk.src;
}

clock_div_t clk_get_smclk_div()
{
    return clk_device_context.smclk.div;
}

unsigned int clk_set_smclk_div(clock_div_t divider)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(divider) {
        case CLK_DIV_1 :    temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__1;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_2 :    temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__2;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_4 :    temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__4;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_8 :    temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__8;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_16 :   temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__16;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_32 :   temp = CSCTL3;
                            temp &= ~(DIVS0 | DIVS1 | DIVS2);
                            temp |= DIVS__32;
                            CSCTL3 = temp;
                            break;
        default :           ret = EBADDIV_CLK;
                            break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.smclk.div = divider;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

unsigned int clk_set_smclk_src(clock_src_t source)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(source){
        case CLK_DCO :  temp = CSCTL2;
                        temp &= ~(SELS0 | SELS1 | SELS2);
                        temp |= SELS__DCOCLK;
                        CSCTL2 = temp;
                        break;
        case CLK_VLO :  temp = CSCTL2;
                        temp &= ~(SELS0 | SELS1 | SELS2);
                        temp |= SELS__VLOCLK;
                        CSCTL2 = temp;
                        break;
        default :       ret = EBADSOURCE_CLK;
                        break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.smclk.src = source;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}


/******************************************************************/
/************************************************** MASTER CLOCK **/

unsigned long clk_get_mclk_speed()
{
    if(clk_device_context.mclk.src == CLK_DCO)
        return clk_device_context.dco_freq / ((unsigned long)clk_device_context.mclk.div);
    else
        return CLK_VLO_NOMINAL_FREQ / ((unsigned long)clk_device_context.mclk.div);
}

clock_src_t clk_get_mclk_src()
{
    return clk_device_context.mclk.src;
}

clock_div_t clk_get_mclk_div()
{
    return clk_device_context.mclk.div;
}

unsigned int clk_set_mclk_div(clock_div_t divider)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(divider) {
        case CLK_DIV_1 :    temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__1;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_2 :    temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__2;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_4 :    temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__4;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_8 :    temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__8;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_16 :   temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__16;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_32 :   temp = CSCTL3;
                            temp &= ~(DIVM0 | DIVM1 | DIVM2);
                            temp |= DIVM__32;
                            CSCTL3 = temp;
                            break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.mclk.div = divider;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

unsigned int clk_set_mclk_src(clock_src_t source)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(source){
        case CLK_DCO :  temp = CSCTL2;
                        temp &= ~(SELM0 | SELM1 | SELM2);
                        temp |= SELM__DCOCLK;
                        CSCTL2 = temp;
                        break;
        case CLK_VLO :  temp = CSCTL2;
                        temp &= ~(SELM0 | SELM1 | SELM2);
                        temp |= SELM__VLOCLK;
                        CSCTL2 = temp;
                        break;
        default :       ret = EBADSOURCE_CLK;
                        break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.mclk.src = source;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}


/******************************************************************/
/*********************************************** AUXILIARY CLOCK **/

unsigned long clk_get_aclk_speed()
{
    if(clk_device_context.amclk.src == CLK_DCO)
        return clk_device_context.dco_freq / ((unsigned long)clk_device_context.amclk.div);
    else
        return CLK_VLO_NOMINAL_FREQ / ((unsigned long)clk_device_context.amclk.div);
}

clock_src_t clk_get_aclk_src()
{
    return clk_device_context.amclk.src;
}

clock_div_t clk_get_aclk_div()
{
    return clk_device_context.amclk.div;
}

unsigned int clk_set_aclk_div(clock_div_t divider)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(divider) {
        case CLK_DIV_1 :    temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__1;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_2 :    temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__2;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_4 :    temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__4;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_8 :    temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__8;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_16 :   temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__16;
                            CSCTL3 = temp;
                            break;
        case CLK_DIV_32 :   temp = CSCTL3;
                            temp &= ~(DIVA0 | DIVA1 | DIVA2);
                            temp |= DIVA__32;
                            CSCTL3 = temp;
                            break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.amclk.div = divider;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

unsigned int clk_set_aclk_src(clock_src_t source)
{
    unsigned int ret = ESUCCESS_CLK;
    register unsigned int temp;
    unlock_clock_system();
    switch(source){
        case CLK_DCO :  temp = CSCTL2;
                        temp &= ~(SELA0 | SELA1 | SELA2);
                        temp |= SELA__DCOCLK;
                        CSCTL2 = temp;
                        break;
        case CLK_VLO :  temp = CSCTL2;
                        temp &= ~(SELA0 | SELA1 | SELA2);
                        temp |= SELA__VLOCLK;
                        CSCTL2 = temp;
                        break;
        default :       ret = EBADSOURCE_CLK;
                        break;
    }
    if(ret == ESUCCESS_CLK){
        clk_device_context.amclk.src = source;
    }
    lock_clock_system();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

