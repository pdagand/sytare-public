/**
 *  \file   drv_tmp.c
 *  \brief  TI FR5739 board lib, temperature sensor
 *  \author Tristan Delizy, from TI msp430fr57xx_adc10_16 exemple
 *  \date   2016
 **/

/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/utils.h>
#include <drivers/clock.h>
#include <drivers/temperature.h>
#include <drivers/dma.h>

//dbg
#include "kernel.h"

/******************************************************************/
/********************************************** INTERNAL DEFINES **/

#define CALADC10_15V_30C  *((unsigned int *)0x1A1A)   // Temperature Sensor Calibration-30 C
#define CALADC10_15V_85C  *((unsigned int *)0x1A1C)   // Temperature Sensor Calibration-85 C


/******************************************************************/
/********************************************************* TYPES **/
// sytare temperature driver data descriptor for persistency
struct tmp_device_context_t
{
    unsigned int                sensed;
    unsigned int                res;
};


/******************************************************************/
/***************************************************** VARIABLES **/

static
syt_dev_ctx_changes_t ctx_changes;

static
struct tmp_device_context_t tmp_device_context;


/******************************************************************/
/********************************************** DRIVER FUNCTIONS **/

/// Turn off temperature sensor to save power
void temp_stop(void)
{
    REFCTL0 |= REFTCOFF;
    REFCTL0 &= ~REFON;
}

static
void tmp_drv_hw_init(void)
{
    // Configure ADC10 - Pulse sample mode; ADC10SC trigger
    ADC10CTL0 = ADC10SHT_8 + ADC10ON;           // 16 ADC10CLKs; ADC ON,temperature sample period>30us
    ADC10CTL1 = ADC10SHP + ADC10CONSEQ_0;       // s/w trig, single ch/conv
    ADC10CTL2 = ADC10RES;                       // 10-bit conversion results
    ADC10MCTL0 = ADC10SREF_1 + ADC10INCH_10;    // ADC input ch A10 => temp sense

    // Configure internal reference
    while(REFCTL0 & REFGENBUSY);                // If ref generator busy, WAIT
    REFCTL0 |= REFVSEL_0+REFON;                 // Select internal ref = 1.5V
                                                // Internal Reference ON
    ADC10IE |=ADC10IE0;                         // enable the Interrupt request for a completed ADC10_B conversion
    ADC10IFG = 0;

    clk_delay_micro(400);                       // Delay for Ref to settle
}

void tmp_drv_init(void)
{
    syt_drv_register(tmp_drv_save, tmp_drv_restore,
                     sizeof(struct tmp_device_context_t));

    tmp_drv_hw_init();

    drv_mark_dirty(&ctx_changes);
}


void tmp_drv_save(int drv_handle)
{
    drv_save(&ctx_changes,
             syt_drv_get_ctx_next(drv_handle),
             &tmp_device_context,
             sizeof(tmp_device_context));
}


void tmp_drv_restore(int drv_handle)
{
    dma_memcpy(&tmp_device_context,
               syt_drv_get_ctx_last(drv_handle),
               sizeof(tmp_device_context));

    tmp_drv_hw_init();

    // mark as dirty to make sure that it will be saved to the new checkpoint
    // that is still uninitialized after driver restoration
    drv_mark_dirty(&ctx_changes);
}

int tmp_drv_sample(void)
{
    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(60);
    // SYT_DBG_PIN_LOW();

    ADC10IE |=ADC10IE0;                         // enable the Interrupt request for a completed ADC10_B conversion
    ADC10IFG = 0;
    ADC10CTL0 |= ADC10ENC + ADC10SC;            // Sampling and conversion start

    __bis_SR_register(LPM3_bits + GIE);         // LPM4 with interrupts enabled
    __no_operation();

    // Temperature in Celsius
    // Test separate cases to avoid overflow
    if(tmp_device_context.sensed <= CALADC10_15V_85C) // Most likely to occur
        tmp_device_context.res = -((CALADC10_15V_85C - tmp_device_context.sensed) * (85-30)/(CALADC10_15V_85C-CALADC10_15V_30C)) + 85;
    else
        tmp_device_context.res = (tmp_device_context.sensed - CALADC10_15V_85C) * (85-30)/(CALADC10_15V_85C-CALADC10_15V_30C) + 85;

    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(60);
    // SYT_DBG_PIN_LOW();

    drv_mark_dirty(&ctx_changes);

    return tmp_device_context.res;
}

// ADC10 interrupt service routine (be carefull to only allow it to trigger inside the OS execution,
// ie. during driver calls, for now)
void __attribute__ ((interrupt(ADC10_VECTOR))) tmp_drv_isr(void)
{
    switch(__even_in_range(ADC10IV,12))
    {
        case  0: break;                             // No interrupt
        case  2: break;                             // conversion result overflow
        case  4: break;                             // conversion time overflow
        case  6: break;                             // ADC10HI
        case  8: break;                             // ADC10LO
        case 10: break;                             // ADC10IN
        case 12: tmp_device_context.sensed = ADC10MEM0;
                 __bic_SR_register_on_exit(LPM3_bits);
                 break;                             // Clear CPUOFF bit from 0(SR)
        default: tmp_device_context.sensed = 0xFF;  //bad value
                 break;
    }
    __bic_SR_register_on_exit(LPM3_bits);
    ADC10IFG = 0;
}
