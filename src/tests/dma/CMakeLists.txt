cmake_minimum_required(VERSION 3.0)
include(../../cmake/Sytare-Tests.cmake)

project(${PROJECT_NAME} C)
sytare_test(${PROJECT_NAME})
