#include <msp430.h>
#include "kernel.h"

int main (void)
{
    WDTCTL = WDTPW | WDTHOLD;

    // spin so we can verify via debugger
    while(1);

    return 0;
}
