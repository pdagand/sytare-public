cmake_minimum_required(VERSION 2.8)
include(../../cmake/Sytare-Tests.cmake)

project(${PROJECT_NAME} C)


add_executable(${PROJECT_NAME} main.c)

add_custom_target(tests
	DEPENDS ${PROJECT_NAME}
	COMMAND
		./check_sections.sh $<TARGET_FILE:${PROJECT_NAME}>
	WORKING_DIRECTORY
		${CMAKE_CURRENT_SOURCE_DIR}
	)
