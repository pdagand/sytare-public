#include <msp430.h>

// to verify placement in sections
static volatile int a = 1;
static volatile int b;

int main (void)
{
    WDTCTL = WDTPW | WDTHOLD;

	// just use variables
	b = 1;
    while(a || b);

    return 0;
}
