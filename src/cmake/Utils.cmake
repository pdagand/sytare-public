macro(include_guard)
	if(DEFINED "_INCLUDE_GUARD_${CMAKE_CURRENT_LIST_FILE}")
		return()
	endif()
	set("_INCLUDE_GUARD_${CMAKE_CURRENT_LIST_FILE}" INCLUDED)
endmacro(include_guard)


# Store full path of all sub directories of DIR in VAR. If relative or absoulte
# depends on DIR.
function(get_subdirs VAR DIR)
  file(GLOB children RELATIVE ${DIR} ${DIR}/*)
  set(dirlist "")
  foreach(child ${children})
    if(IS_DIRECTORY ${DIR}/${child})
      list(APPEND dirlist ${DIR}/${child})
    endif()
  endforeach()
  set(${VAR} ${dirlist} PARENT_SCOPE)
endfunction(get_subdirs)


macro(target_dump_listing TARGET)
	add_custom_command(TARGET ${TARGET}
		COMMAND
		    msp430-elf-objdump -zDhstS $<TARGET_FILE:${TARGET}> > $<TARGET_FILE:${TARGET}>.lst)
endmacro(target_dump_listing)


# provide a make target to program the binary to MSP430 board and debug
# usage: `make program-TARGETNAME`
#        `make debug-TARGETNAME`
macro(target_program TARGET)
	add_custom_target(program-${TARGET}
		DEPENDS
		    ${TARGET}
		COMMAND
		    mspdebug rf2500 'prog $<TARGET_FILE:${TARGET}>'
		COMMAND
		    ${CMAKE_COMMAND} -E echo 'Binary size:'
		COMMAND
		    msp430-elf-size $<TARGET_FILE:${TARGET}>)

	add_custom_target(debug-${TARGET}
		DEPENDS
		    ${TARGET}
		COMMAND
		    ${CMAKE_COMMAND} -E echo sym import $<TARGET_FILE:${TARGET}> > .mspdebug
		COMMAND
		    mspdebug rf2500)

	if(SYTARE_APPLICATION)
		# add alias target 'program' if run inside an application
		add_custom_target(program DEPENDS program-${TARGET})
	endif()
endmacro(target_program)


# rename a section of a target
macro(target_rename_section TARGET BEFORE AFTER)
	add_custom_command(
		TARGET ${TARGET} POST_BUILD
		COMMAND
		    ${CMAKE_OBJCOPY} --rename-section ${BEFORE}=${AFTER} $<TARGET_FILE:${TARGET}>)
endmacro(target_rename_section)
