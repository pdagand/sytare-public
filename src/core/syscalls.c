/**
 *  \file   syscalls.c
 *  \brief  TI FR5739 board lib, clock system
 *  \author Tristan Delizy
 *  \date   2016
 **/


/* remark : we use here "syt_syscall_ptr" which points
 * on syt_checkpoints[1-syt_current_chkpt].syscall.
 * it's value is set correctly at each boot.
 * we have no choice here because the array desindexing and
 * structure access would require the use of work registers
 * we cannot afford in syscalls.
 */


/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/utils.h>
#include <drivers/clock.h>
#include <drivers/spi.h>
#include <drivers/cc2500.h>
#include <drivers/temperature.h>
#include <drivers/port.h>
#include "kernel.h"


/******************************************************************/
/******************************************************** MACROS **/

static inline __attribute__((always_inline))
void syt_syscall_ctx_switch(void)
{
    // save processor state
    __asm__ __volatile__("push r2 \n\t"
                         : // no outputs
                         : // no inputs
                         );
    // then ensure disabling interrupts for OS operations
    _disable_interrupts();

    //save GPRs (R[4-15])
    __asm__ __volatile__("pushm #12, r15 \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_run_os_stack();

    // save PC in a fram variable
    __asm__ __volatile__("mov %0, r10 \n\t"
                         "mov r0, @r10 \n\t"
                         :
                         : "m"(syt_syscall_ptr)
                         );

    // re-enable interrupts for driver function call
    _enable_interrupts();
}


static inline __attribute__((always_inline))
void syt_syscall_return(void)
{
    // at driver return disable again interrupts for os operations
    _disable_interrupts();

    __asm__ __volatile__("pushm #4, r15 \n\t"
                         : // no outputs
                         : // no inputs
                         );

    void drv_save_all(void);
    drv_save_all();

    __asm__ __volatile__("popm #4, r15 \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_run_usr_stack();

    // clear the syscall reference
    __asm__ __volatile__("mov %0, r10 \n\t"
                         "mov #0, @r10 \n\t"
                         :
                         : "m"(syt_syscall_ptr)
                         );


    // repopulate callee saved registers
    // clear the stack (#8 = 4 regs not restored)
    // restore SR
    __asm__ __volatile__("popm #8, r11 \n\t"
                         "add #8, r1 \n\t"
                         "pop r2 \n\t"
                         : // no outputs
                         : // no inputs
                         );

    // then return
    __asm__ __volatile__("ret \n\t"
                         : // no outputs
                         : // no inputs
                         );
}


/******************************************************************/
/***************************************************** CLOCK_DRV **/
void syt_clk_drv_init(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_drv_init \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_clk_delay_ms(unsigned int delay)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_delay_ms \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned int syt_clk_dummy_call(unsigned int test1, unsigned int test2, unsigned int test3, unsigned int test4)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_dummy_call \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned long syt_clk_get_dco_freq(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_dco_freq \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_dco_freq(unsigned long frequency)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_dco_freq \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned long syt_clk_get_smclk_speed(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_smclk_speed \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_smclk_src(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_smclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_smclk_div(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_smclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_smclk_div(unsigned int divisor)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_smclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_smclk_src(unsigned int source)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_smclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned long syt_clk_get_mclk_speed(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_mclk_speed \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_mclk_src(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_mclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_mclk_div(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_mclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_mclk_div(unsigned int divisor)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_mclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_mclk_src(unsigned int source)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_mclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned long syt_clk_get_aclk_speed(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_aclk_speed \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_aclk_src(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_aclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_get_aclk_div(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_get_aclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_aclk_div(unsigned int divisor)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_aclk_div \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_clk_set_aclk_src(unsigned int source)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #clk_set_aclk_src \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}




/******************************************************************/
/******************************************************* SPI_DRV **/
void syt_spi_init(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_init \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_configure(SPI_SETTINGS const *cfg)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_configure \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned int syt_spi_is_recieving(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_is_recieving \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_spi_select_slave(unsigned int device)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_select_slave \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_spi_deselect_slave(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_deselect_slave \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_read_byte(unsigned char* dst)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_read_byte \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_read_frame(unsigned char* dst, unsigned int len)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_read_frame \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_write_byte(const unsigned char* src)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #syt_spi_write_byte \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_write_frame(const unsigned char* src, unsigned int len)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_write_frame \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_transfert_byte(const unsigned char* src, unsigned char* dst)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_transfert_byte \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_spi_transfert_frame(const unsigned char* src, unsigned char* dst, unsigned int len)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #spi_transfert_frame \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}




/******************************************************************/
/******************************************************** RF_DRV **/
void syt_cc2500_reset(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_reset \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned int syt_cc2500_init(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_init \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_cc2500_configure(RF_SETTINGS const * cfg, int reset_state)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_configure \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_cc2500_rx_register_cb(cc2500_cb_t callback)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_rx_register_cb \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_cc2500_rx_register_buffer(volatile unsigned char * buffer, unsigned char length)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_rx_register_buffer \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_cc2500_set_channel(unsigned char chan)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_set_channel \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_cc2500_calibrate(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_calibrate \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned int syt_cc2500_send_packet(const unsigned char *buffer, const unsigned char length)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_send_packet \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_cc2500_idle(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_idle \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_cc2500_sleep(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_sleep \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_cc2500_wakeup(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_wakeup \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_cc2500_rx_enter(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_rx_enter \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned int syt_cc2500_get_drv_mode(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_get_drv_mode \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

int syt_cc2500_cca(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_cca \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned char syt_cc2500_get_rssi(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #cc2500_get_rssi \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

/******************************************************************/
/******************************************************* TMP_DRV **/
void syt_tmp_drv_init(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #tmp_drv_init \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

int syt_tmp_drv_sample(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #tmp_drv_sample \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}


/******************************************************************/
/******************************************************* PRT_DRV **/

void syt_prt_drv_init(void)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_drv_init \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_out_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_out_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_out_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_out_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_out_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_out_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

unsigned char syt_prt_in_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_in_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_dir_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_dir_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_dir_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_dir_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_dir_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_dir_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_sel0_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel0_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_sel0_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel0_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_sel0_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel0_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_sel1_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel1_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_sel1_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel1_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_sel1_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_sel1_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_ie_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ie_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_ie_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ie_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_ie_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ie_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_ies_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ies_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_ies_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ies_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_ies_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ies_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_ifg_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ifg_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_ifg_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ifg_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_ifg_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ifg_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}

void syt_prt_ren_bis(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ren_bis \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

void syt_prt_ren_bic(unsigned char port, unsigned char mask)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ren_bic \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return;
}

unsigned char syt_prt_ren_get(unsigned char port)
{
    syt_syscall_ctx_switch();

    // call function
    __asm__ __volatile__("call #prt_ren_get \n\t"
                         : // no outputs
                         : // no inputs
                         );

    syt_syscall_return();

    // dead code to avoid gcc warnings/errors
    return 0;
}
