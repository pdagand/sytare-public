
/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "memory_map.h"
#include "config.h"
#include "kernel.h"

#include <drivers/utils.h>
#include <drivers/dma.h>
#include <drivers/led.h>


// stack pointers
size_t syt_os_sp;
size_t syt_usr_sp;


__attribute__((noreturn))
void syt_kernel_panic(void)
{
    // re-init the platform just in case
    WDTCTL = WDTPW + WDTHOLD;

    leds_init();
    leds_on();
    _disable_interrupts();
    for(;;) {
        __asm__ __volatile__(   "nop\n\t"
                                : // no output
                                : // no input
                                );
    }

}


void syt_boot(void)
{
    uint16_t reg_clk = 0;

    // Disabling the watchdog (watchdog state may be restored later)
    WDTCTL = WDTPW + WDTHOLD;

    // Set the default system speed for master clock (24MHz)
    // note that mclock source is DCO by default.
    // Unlock clock system
    CSCTL0_H = (0xA5);

    // set DCO speed to 24MHz
    reg_clk = CSCTL1;
    reg_clk &= ~( DCOFSEL0 | DCOFSEL1);
    reg_clk |= (DCORSEL | DCOFSEL_3);
    CSCTL1 = reg_clk;

    // set mclk div to 1
    reg_clk = CSCTL3;
    reg_clk &= ~(DIVM0 | DIVM1 | DIVM2);
    reg_clk |= DIVM__1;
    CSCTL3 = reg_clk;

    // lock back clock system
    CSCTL0_H = 0;

    // configure finished signal pin as output and set low
    PXDIR(SYT_FINISHED_SIG_PORT) |= SYT_FINISHED_SIG_BITMASK;
    PXOUT(SYT_FINISHED_SIG_PORT) &= ~(SYT_FINISHED_SIG_BITMASK);


    // OS init -------------------------------------------------------//

    // Initialize internal stack pointers
    syt_os_sp  = SYT_OS_STACK  + SYT_OS_STACK_SIZE;
    syt_usr_sp = SYT_USR_STACK + SYT_USR_STACK_SIZE;

    // Moving to the user stack
    syt_run_usr_stack();

    // setup C memory sections
    dma_memset((void*) SYT_OS_BSS,  0, SYT_OS_BSS_SIZE);
    dma_memset((void*) SYT_DRV_BSS, 0, SYT_DRV_BSS_SIZE);
    dma_memset((void*) SYT_USR_BSS, 0, SYT_USR_BSS_SIZE);

    // os_data stays in NVRAM
    dma_memcpy((void*) SYT_DRV_DATA,    (void*) SYT_DRV_DATA_ROM,   SYT_DRV_DATA_SIZE);
    dma_memcpy((void*) SYT_USR_DATA,    (void*) SYT_USR_DATA_ROM,   SYT_USR_DATA_SIZE);


    _enable_interrupts();

    // Calling the user main()
    // needed because of stack change
    int main(void);
    main();

    // send finished signal
    PXOUT(SYT_FINISHED_SIG_PORT) |= SYT_FINISHED_SIG_BITMASK;

    // we're done
    _disable_interrupts();
    while(1);
}


void syt_drv_commit(void) {}
const void* syt_drv_get_ctx_last(int handle) {}
void* syt_drv_get_ctx_next(int handle) {}
int drv_register(drv_save_func_t save, drv_restore_func_t restore, size_t ctx_size) {}
const void* syt_drv_get_last_ctx(int handle) {}
void* syt_drv_get_next_ctx(int handle) {}

// may be used by some applications
uint16_t syt_stats_boot_count(void) { return 1; }
uint16_t syt_stats_failed_chkpt_count(void) { return 0; }
