/**
 *  \file   kernel.c
 *  \brief  Sytare OS startup and routines for
 *          program and drivers persistance
 *  \author Tristan Delizy
 *  \from   nvram-os
 *  \date   2015
 **/

/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "memory_map.h"
#include "config.h"
#include "kernel.h"

#include <drivers/utils.h>
#include <drivers/dma.h>
#include <drivers/led.h>
#include <drivers/comparator.h>

/******************************************************************/
/*********************************************** TYPES & STRUCTS **/

typedef struct
{
    size_t sp;          ///< stack pointer of userland
    size_t syscall;     ///< program counter when entering syscall
    size_t base_addr;   ///< base address of checkpoint section in memory
} syt_checkpoint_t;


typedef struct {
    drv_save_func_t     save;       ///< callback for saving driver state
    drv_restore_func_t  restore;    ///< callback for restoring driver state
    size_t              ctx_offset; ///< offset in device context region
} driver_t;


/******************************************************************/
/******************************************************** MACROS **/

/// Find the offset of a variable that has been allocated in the .checkpoint
/// section (argument is the address of the variable). The linker will always
/// allocate the variable in the first checkpoint, and since every symbol in C
/// can only have one address, we can only access that variable in another
/// checkpoint by it's offset (which is the same in every checkpoint).
#define GET_OFFSET_IN_CHECKPOINT(addr) ( (size_t) addr - SYT_CHECKPOINT0_START )

/// Reference a variable in the .checkpoint section (read as well as write).
///
/// See GET_OFFSET_IN_CHECKPOINT for more information on why this is needed.
#define GET_CHECKPOINT_VAR(name, type, checkpoint_base_addr) \
    ((type*)( (size_t) checkpoint_base_addr + GET_OFFSET_IN_CHECKPOINT(&name) ))


/******************************************************************/
/***************************************************** VARIABLES **/

// pointer on the syscall container
// needed to avoid registers corruption in syscalls
// initialized at each boot to the adress where the syscall address will be
// stored inside the next checkpoint
size_t syt_syscall_ptr;


// Driver handling -----------------------------------------------//

// number of drivers that have already registered themselves
static size_t drivers_registered = 0;

// Offset in device context region inside checkpoint for the next drivers. In
// other words points to the beginning of free memory.
static size_t drivers_next_offset = 0;

// keep track of registered drivers
static driver_t drivers[SYT_MAX_DRIVERS];


// Checkpointing status variables --------------------------------//
// /!\ these must never be changed outside the primary OS routines.
static int syt_first_boot = 1;

// metadata about checkpoints that has to be stored in each checkpoint, it has
// to be allocated in the .checkpoint section
static __attribute__((section(".checkpoint")))
syt_checkpoint_t syt_checkpoint;

// assign at boot to checkpoint{0,1}
static syt_checkpoint_t * syt_last = 0;
static syt_checkpoint_t * syt_next = 0;

// stack pointers
size_t syt_os_sp;
size_t syt_usr_sp;

// statistics
static uint16_t stats_boot_count = 0;
static uint16_t stats_success_count = 0;


/******************************************************************/
/**************************************** FUNCTIONS DECLARATIONS **/

// Application main() function to be called to start the user firmware
int main(void);

static void syt_shutdown_platform(void);

static inline bool syt_is_first_boot(void);

/******************************************************************/
/********************************************** DRIVER INTERFACE **/

const void* syt_drv_get_ctx_last(int handle)
{
    return (void*) (syt_last->base_addr +
                    SYT_CHECKPOINT_DEV_CTX_OFFSET +
                    drivers[handle].ctx_offset);
}


void* syt_drv_get_ctx_next(int handle)
{
    return (void*) (syt_next->base_addr +
                    SYT_CHECKPOINT_DEV_CTX_OFFSET +
                    drivers[handle].ctx_offset);
}


/// Private function to register a driver to the kernel. It will be wrapped by
/// public macro syt_drv_register() with same prototype to allocate the context
/// memory on-the-fly, see include/drivers/utils.h
int drv_register(drv_save_func_t save, drv_restore_func_t restore, size_t ctx_size)
{
    const size_t max_drivers = sizeof(drivers) / sizeof(*drivers);
    if(drivers_registered >= max_drivers) {
        syt_kernel_panic();
        return -1;
    }

    // assign new ID and register driver
    const size_t id = drivers_registered++;
    drivers[id] = (driver_t) {
        .save = save,
        .restore = restore,
        .ctx_offset = drivers_next_offset
    };

    // advance offset for next driver
    drivers_next_offset += ctx_size;

    return (int) id;
}


/// Call the restoration function of each driver registered for persistency
static
void drv_restore_all(void)
{
    // restore each driver that has a restore function
    for(size_t id = 0; id < drivers_registered; id++) {
        if(drivers[id].restore != NULL) {
            drivers[id].restore((int) id);
        }
    }
}


/// Call the saving function of each driver registered for persistency
void drv_save_all(void)
{
    // save each driver that has a save function
    for(size_t id = 0; id < drivers_registered; id++) {
        if(drivers[id].save != NULL) {
            drivers[id].save((int) id);
        }
    }
}


/******************************************************************/
/**************************************** CHECKPOINTING ROUTINES **/

static
void syt_save_user_state(syt_checkpoint_t* checkpoint)
{
    dma_memcpy((void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_STACK_OFFSET),
               (void*) SYT_USR_STACK,
               SYT_USR_STACK_SIZE);

    dma_memcpy((void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_DATA_OFFSET),
               (void*) SYT_USR_DATA,
               SYT_USR_DATA_SIZE);

    dma_memcpy((void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_BSS_OFFSET),
               (void*) SYT_USR_BSS,
               SYT_USR_BSS_SIZE);

    // Save user stack pointer
    checkpoint->sp = syt_usr_sp;
}


static
void syt_restore_user_state(syt_checkpoint_t* checkpoint)
{
    dma_memcpy((void*) SYT_USR_STACK,
               (void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_STACK_OFFSET),
               SYT_USR_STACK_SIZE);

    dma_memcpy((void*) SYT_USR_DATA,
               (void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_DATA_OFFSET),
               SYT_USR_DATA_SIZE);

    dma_memcpy((void*) SYT_USR_BSS,
               (void*) (checkpoint->base_addr + SYT_CHECKPOINT_USR_BSS_OFFSET),
               SYT_USR_BSS_SIZE);

    // Restore user stack pointer
    syt_usr_sp = checkpoint->sp;
}

static
void syt_create_checkpoint(void)
{
    // Assumption:
    // this function is called from kernel stack, i.e. syt_usr_sp is valid
    syt_save_user_state(syt_next);

    syt_last = syt_next;		// change the active checkpoint
    stats_success_count++;		// statistics
}

static
void syt_restore_checkpoint(void)
{
    // Initialize the next checkpoint. The syscall pointer is checked inside the
    // checkpoint ISR to determine on which stack we're executing. Not setting
    // it here will cause wrong checkpointing if we loose power during a retried
    // syscall.
    syt_next->syscall = syt_last->syscall;

    syt_restore_user_state(syt_last);
    drv_restore_all();

    // restoring will make some drivers (probably unneccessarily) dirty, so save
    // right now to start with clean drivers
    drv_save_all();
}

/******************************************************************/
/********************************************* SYSTEM PRIMITIVES **/

// function used to forgot previously saved state
static void syt_reset_to_first_boot(void)
{
    // to avoid broken time machine issue
    // syt_syt_current_chkpt must NOT be changed first !

    // GS-2016-07-22-12:25 TODO: fix the comment above to match the
    // renaming of "current_checkpoint" into "next" and "last"
    syt_first_boot = 1;
    syt_next = 0;
    syt_last = 0;
    stats_boot_count = 0;
    stats_success_count = 0;

    drivers_registered = 0;
    drivers_next_offset = 0;
}

// function that returns true when a jumper is placed between pins X.x and Y.y or
// falseotherwise
static bool syt_is_jumper_placed(void)
{
    // we use a jumper placed between pins X.x and pin Y.y to reset the
    // board in its initial state. (forgot all previous valid and corrupted
    // checkpoints)

    // pin configuration for test from X.x to Y.y
    PXDIR(SYT_HRST_PIN_1) &= ~(BITX(SYT_HRST_BIT_1));
    PXREN(SYT_HRST_PIN_1) |= BITX(SYT_HRST_BIT_1);
    PXOUT(SYT_HRST_PIN_1) |= BITX(SYT_HRST_BIT_1);

    PXDIR(SYT_HRST_PIN_2) |= BITX(SYT_HRST_BIT_2);

    // test the jumper presence with logical level 1
    PXOUT(SYT_HRST_PIN_2) |= BITX(SYT_HRST_BIT_2);
    if(PXIN(SYT_HRST_PIN_1) & BITX(SYT_HRST_BIT_1)) {

        // test the jumper presence with logical level 0
        PXOUT(SYT_HRST_PIN_2) &= ~(BITX(SYT_HRST_BIT_2));
        if(!(PXIN(SYT_HRST_PIN_1) & BITX(SYT_HRST_BIT_1))) {

            // pin configuration for test from Y.y to X.x
            PXDIR(SYT_HRST_PIN_2) &= ~(BITX(SYT_HRST_BIT_2));
            PXREN(SYT_HRST_PIN_2) |= BITX(SYT_HRST_BIT_2);
            PXOUT(SYT_HRST_PIN_2) |= BITX(SYT_HRST_BIT_2);

            PXDIR(SYT_HRST_PIN_1) |= BITX(SYT_HRST_BIT_1);

            // test the jumper presence with logical level 1
            PXOUT(SYT_HRST_PIN_1) |= BITX(SYT_HRST_BIT_1);
            if(PXIN(SYT_HRST_PIN_2) & BITX(SYT_HRST_BIT_2)) {

                // test the jumper presence with logical level 0
                PXOUT(SYT_HRST_PIN_1) &= ~(BITX(SYT_HRST_BIT_1));
                if(!(PXIN(SYT_HRST_PIN_2) & BITX(SYT_HRST_BIT_2)))
                    return true;
            }
        }
    }

    return false;
}

// endless loop for now
void syt_shutdown_platform(void)
{
    for(;;) {
        __asm__ __volatile__(   "nop\n\t"
                                : // no output
                                : // no input
                                );
    }
}


__attribute__((noreturn))
void syt_kernel_panic(void)
{
    // re-init the platform just in case
    WDTCTL = WDTPW + WDTHOLD;

    leds_init();
    leds_on();
    _disable_interrupts();
    for(;;) {
        __asm__ __volatile__(   "nop\n\t"
                                : // no output
                                : // no input
                                );
    }

}


/******************************************************************/
/********************************** SYSTEM STATISTICS MONITORING **/

uint16_t syt_stats_boot_count(void)
{
    return stats_boot_count;
}

uint16_t syt_stats_completed_chkpt_count(void)
{
    return stats_success_count;
}

uint16_t syt_stats_failed_chkpt_count(void)
{
    return (stats_boot_count - stats_success_count - 1);
}


/******************************************************************/
/*********************************************************** ISR **/

// trigger for checkpoint creation
__attribute__ ((interrupt(COMP_D_VECTOR)))
void syt_checkpoint_trigger_isr(void)
{
    comp_acknowledge_interrupt();

    // if we are not already running on OS stack, switch to it.
    if(syt_next->syscall == 0)
        syt_run_os_stack();

    syt_create_checkpoint();

    // shutdown platform to save power.
    syt_shutdown_platform();

    // if we do not shut down the platform we restore application context.
    syt_run_usr_stack();
}


/******************************************************************/
/*************************************************** SYSTEM BOOT **/

bool syt_is_first_boot(void)
{
    return (syt_first_boot == 1);
}

// Sets the beginning of OS execution, after program data initialization
// section insered by the linker script inside symbol <__msp430_init>
void syt_boot(void)
{
    size_t syscall_restore_buffer = 0;
    uint16_t reg_clk = 0;
// platform level init -------------------------------------------//

    // Disabling the watchdog (watchdog state may be restored later)
    WDTCTL = WDTPW + WDTHOLD;

    // Set the default system speed for master clock (24MHz)
    // note that mclock source is DCO by default.
    // Unlock clock system
    CSCTL0_H = (0xA5);

    // set DCO speed to 24MHz
    reg_clk = CSCTL1;
    reg_clk &= ~( DCOFSEL0 | DCOFSEL1);
    reg_clk |= (DCORSEL | DCOFSEL_3);
    CSCTL1 = reg_clk;

    // set mclk div to 1
    reg_clk = CSCTL3;
    reg_clk &= ~(DIVM0 | DIVM1 | DIVM2);
    reg_clk |= DIVM__1;
    CSCTL3 = reg_clk;

    // lock back clock system
    CSCTL0_H = 0;

#if SYT_LOW_LEVEL_DBG == 1
    // configure output debug pin
    PXOUT(SYT_DBG_PIN)  &= ~(BITX(SYT_DBG_BIT));
    PXDIR(SYT_DBG_PIN)  |= (BITX(SYT_DBG_BIT));
    PXSEL0(SYT_DBG_PIN) &= ~(BITX(SYT_DBG_BIT));
    PXSEL1(SYT_DBG_PIN) &= ~(BITX(SYT_DBG_BIT));
#endif

    // configure finished signal pin as output and set low
    PXDIR(SYT_FINISHED_SIG_PORT) |= SYT_FINISHED_SIG_BITMASK;
    PXOUT(SYT_FINISHED_SIG_PORT) &= ~(SYT_FINISHED_SIG_BITMASK);

    // configure reset signal pin as input and enable pull-down
    PXDIR(SYT_RESET_SIG_PORT) &= ~(SYT_RESET_SIG_BITMASK);
    PXREN(SYT_RESET_SIG_PORT) |= SYT_RESET_SIG_BITMASK;
    PXOUT(SYT_RESET_SIG_PORT) &= ~(SYT_RESET_SIG_BITMASK);


    // OS Checks  ----------------------------------------------------//

    // check if a reset is requested by jumper or reset signal
    if( syt_is_jumper_placed() ||
        (PXIN(SYT_RESET_SIG_PORT) & SYT_RESET_SIG_BITMASK) ) {
        syt_reset_to_first_boot();

        // stop execution here
        // TODO: signalize to user!
        syt_shutdown_platform();
    }

    // disable pull down again so this pin could be used otherwise in theory
    PXREN(SYT_RESET_SIG_PORT) &= ~(SYT_RESET_SIG_BITMASK);

    // check of broken state
    if(!syt_first_boot && syt_last == 0)
        syt_reset_to_first_boot();


    // OS init -------------------------------------------------------//

    syt_checkpoint_t* syt_checkpoint0 = GET_CHECKPOINT_VAR(syt_checkpoint, syt_checkpoint_t, SYT_CHECKPOINT0_START);
    syt_checkpoint_t* syt_checkpoint1 = GET_CHECKPOINT_VAR(syt_checkpoint, syt_checkpoint_t, SYT_CHECKPOINT1_START);

    if(syt_is_first_boot()) {
        // Preparing OS bss section in NVRAM at first boot
        dma_memset((void *) SYT_OS_BSS, 0, SYT_OS_BSS_SIZE);

        // Clear the checkpointing metadata structure and the actual checkpoints
        dma_memset(syt_checkpoint0, 0, sizeof(syt_checkpoint_t));
        dma_memset(syt_checkpoint1, 0, sizeof(syt_checkpoint_t));
        dma_memset((void*) SYT_CHECKPOINT0_START, 0, SYT_CHECKPOINT_SIZE);
        dma_memset((void*) SYT_CHECKPOINT1_START, 0, SYT_CHECKPOINT_SIZE);

        // initialize base addresses
        syt_checkpoint0->base_addr = SYT_CHECKPOINT0_START;
        syt_checkpoint1->base_addr = SYT_CHECKPOINT1_START;
    }

    // Initialize the checkpoint pointers
    if(syt_is_first_boot()) {
        // first run: just initialize the next pointer
        syt_next = syt_checkpoint0;
    } else {
        // normal run: initialize next pointer to the other memory zone than the
        //             one pointed by last
        syt_next = (syt_last == syt_checkpoint0) ?
                        syt_checkpoint1 :
                        syt_checkpoint0;
    }

    // statistics
    stats_boot_count++;

    // Initialize the syscall pointer
    syt_syscall_ptr = (size_t) &(syt_next->syscall);

    // Start the comparator for power loss detection
    comp_init(true);                // init comparator with interrupts enabled
    comp_set_res(SYT_COMP_D_STEP);  // set the resistor ladder step
    comp_start();                   // start the compartator

    // launch program execution --------------------------------------//

    // initialize memory for device drivers :
    // cannot be skipped due to non persistent drivers

    // Load .drv_data in RAM
    dma_memcpy((void*) SYT_DRV_DATA, (void*) SYT_DRV_DATA_ROM, SYT_DRV_DATA_SIZE);

    // Clear .drv_bss section in RAM
    dma_memset((void*) SYT_DRV_BSS, 0, SYT_DRV_BSS_SIZE);

    // check whether first_boot or reboot
    if(syt_is_first_boot()) {
        syt_first_boot = 0;

        // ------------------------------------------------------------------ >>
        // no valid checkpoint to restore so we start first program execution >>
        // ------------------------------------------------------------------ >>

        // Preparing user data and bss section in RAM
        // load .usr_data in RAM
        dma_memcpy((void *) SYT_USR_DATA, (void *) SYT_USR_DATA_ROM, SYT_USR_DATA_SIZE);

        // Clear .usr_bss section in RAM
        dma_memset((void *) SYT_USR_BSS, 0, SYT_USR_BSS_SIZE);

        // Initialize user stack pointer
        syt_usr_sp = SYT_USR_STACK + SYT_USR_STACK_SIZE - 2;

        // Moving to the user stack
        syt_run_usr_stack();

        _enable_interrupts();

        // Calling the user main()
        // needed because of stack change
        main();

    } else {

        // --------------------------------------------------------------------- >>
        // previous program execution produced a checkpoint we're gonna restore. >>
        // --------------------------------------------------------------------- >>

        syt_restore_checkpoint();

        // check if we were interrupted during syscall
        if(syt_last->syscall == 0) {

            // ----------------------------------------------------------------------- >>
            // interrupted during program execution, we can directly resume execution. >>
            // ----------------------------------------------------------------------- >>

            syt_run_usr_stack();

            // we need to restore the registers pushed onto the stack when
            // entered in the ISR where we created checkpoint before returning
            // to the previously executed program
            __asm__ __volatile__(   "popm #12,r15   \n\t" // restore GPRs
                                    "reti           \n\t" // return from interrupt
                                    : // no output
                                    : // no input
                                    );

        } else {

            // -------------------------------------------------------- >>
            // interrupted during a syscall, we will retry driver call. >>
            // -------------------------------------------------------- >>

            syscall_restore_buffer = syt_last->syscall;

            // first we need to repopulate the registers used to pass arguments to the syscall
            // we do not want to pop them as they need to stay in stack if the call fails again.
            // the GPRs r3-r15 have been pushed onto the stack so r12-r15 are not at the usr_sp index.
            __asm__ __volatile__(   "mov %0,      r15     \n\t"
                                    "mov 16(r15), r12     \n\t"
                                    "mov 18(r15), r13     \n\t"
                                    "mov 20(r15), r14     \n\t"
                                    "mov 22(r15), r15     \n\t"
                                    : // no output
                                    : "m" (syt_usr_sp)
                                    );

            // branching to the start of the interrupted syscall
            // /!\ using a pointer or an array indexed content here will possibly corrupt some of the work
            // registers (r12 - r15) that we restored above. (we use syscall_restore_buffer for this reason)
            __asm__ __volatile__(   "mov %0, r0     \n\t"
                                    : // no output
                                    : "m" (syscall_restore_buffer)
                                    );
        }
    }


    // ... main() has terminated now

    // send finished signal
    PXOUT(SYT_FINISHED_SIG_PORT) |= SYT_FINISHED_SIG_BITMASK;

    // if we return from the main, execution is over.
    syt_reset_to_first_boot();
    syt_shutdown_platform();
}
