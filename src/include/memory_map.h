/**
 *  \file   memory_map.h
 *  \brief  Sytare project memory layout file (link between code and lonker script)
 *  \author Tristan Delizy
 *  \from   nvram-os
 *  \date   2015
 **/

#ifndef HEADER_MEMORY_LAYOUT
#define HEADER_MEMORY_LAYOUT

/* This file contains definitions of some variables set in the
 * linker scripts. These values will only be computed at link
 * time. For more detail, see sytare.ld
 */


/******************************************************************/
/************************************************* SYSTEM STACKS **/
extern char LINKER_syt_usr_stack_start;
extern char LINKER_syt_usr_stack_size;
extern char LINKER_syt_os_stack_start;
extern char LINKER_syt_os_stack_size;

#define SYT_PROG_CHKPT SYT_USR_STACK

#define SYT_USR_STACK       ((unsigned int) &LINKER_syt_usr_stack_start)
#define SYT_USR_STACK_SIZE  ((unsigned int) &LINKER_syt_usr_stack_size)

#define SYT_OS_STACK        ((unsigned int) &LINKER_syt_os_stack_start)
#define SYT_OS_STACK_SIZE   ((unsigned int) &LINKER_syt_os_stack_size)


/******************************************************************/
/********************************************** OS MEMORY REGION **/
extern char LINKER_syt_os_bss_start;
extern char LINKER_syt_os_bss_size;

#define SYT_OS_BSS          ((unsigned int) &LINKER_syt_os_bss_start)
#define SYT_OS_BSS_SIZE     ((unsigned int) &LINKER_syt_os_bss_size)


/******************************************************************/
/******************************************** USER MEMORY REGION **/
extern char LINKER_syt_usr_data_start;
extern char LINKER_syt_usr_data_start_rom;
extern char LINKER_syt_usr_data_size;
extern char LINKER_syt_usr_bss_start;
extern char LINKER_syt_usr_bss_size;

#define SYT_USR_DATA        ((unsigned int) &LINKER_syt_usr_data_start)
#define SYT_USR_DATA_ROM    ((unsigned int) &LINKER_syt_usr_data_start_rom)
#define SYT_USR_DATA_SIZE   ((unsigned int) &LINKER_syt_usr_data_size)
#define SYT_USR_BSS         ((unsigned int) &LINKER_syt_usr_bss_start)
#define SYT_USR_BSS_SIZE    ((unsigned int) &LINKER_syt_usr_bss_size)


/******************************************************************/
/***************************************** DRIVERS MEMORY REGION **/
extern char LINKER_syt_drv_data_start;
extern char LINKER_syt_drv_data_start_rom;
extern char LINKER_syt_drv_data_size;
extern char LINKER_syt_drv_bss_start;
extern char LINKER_syt_drv_bss_size;

#define SYT_DRV_DATA        ((unsigned int) &LINKER_syt_drv_data_start)
#define SYT_DRV_DATA_ROM    ((unsigned int) &LINKER_syt_drv_data_start_rom)
#define SYT_DRV_DATA_SIZE   ((unsigned int) &LINKER_syt_drv_data_size)
#define SYT_DRV_BSS         ((unsigned int) &LINKER_syt_drv_bss_start)
#define SYT_DRV_BSS_SIZE    ((unsigned int) &LINKER_syt_drv_bss_size)


/******************************************************************/
/************************************** CHECKPOINT MEMORY REGION **/

extern char LINKER_syt_checkpoint0_start;
extern char LINKER_syt_checkpoint1_start;
extern char LINKER_syt_checkpoint_size;

#define SYT_CHECKPOINT0_START   ((size_t) &LINKER_syt_checkpoint0_start)
#define SYT_CHECKPOINT1_START   ((size_t) &LINKER_syt_checkpoint1_start)
#define SYT_CHECKPOINT_SIZE     ((size_t) &LINKER_syt_checkpoint_size)

extern char LINKER_syt_checkpoint_usr_stack_offset;
extern char LINKER_syt_checkpoint_usr_data_offset;
extern char LINKER_syt_checkpoint_usr_bss_offset;

#define SYT_CHECKPOINT_USR_STACK_OFFSET ((size_t) &LINKER_syt_checkpoint_usr_stack_offset)
#define SYT_CHECKPOINT_USR_DATA_OFFSET  ((size_t) &LINKER_syt_checkpoint_usr_data_offset)
#define SYT_CHECKPOINT_USR_BSS_OFFSET   ((size_t) &LINKER_syt_checkpoint_usr_bss_offset)

extern char LINKER_syt_checkpoint_dev_ctx_start;
extern char LINKER_syt_checkpoint_dev_ctx_end;
extern char LINKER_syt_checkpoint_dev_ctx_offset;

#define SYT_CHECKPOINT_DEV_CTX_START    ((size_t) &LINKER_syt_checkpoint_dev_ctx_start)
#define SYT_CHECKPOINT_DEV_CTX_END      ((size_t) &LINKER_syt_checkpoint_dev_ctx_end)
#define SYT_CHECKPOINT_DEV_CTX_OFFSET   ((size_t) &LINKER_syt_checkpoint_dev_ctx_offset)

#endif
