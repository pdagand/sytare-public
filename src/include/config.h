/**
 *  \file   os_config.h
 *  \brief  Sytare project os configuration file
 *  \author Tristan Delizy
 *  \from   nvram-os
 *  \date   2015
 **/


/* OS parameters */
#ifndef HEADER_OS_CONFIG
#define HEADER_OS_CONFIG

#include "memory_map.h"

/// Max number of drivers that can be used at the same time. Can be overwritten
/// by user.
#ifndef SYT_MAX_DRIVERS
#define SYT_MAX_DRIVERS     (8)
#endif

/* Defines the threshold of Comparator D trigger*/
#define SYT_COMP_D_STEP             (21)    /* the voltage ref is applied to a 32 step resistor ladder
                                            determining at which fraction of ref voltage (1.5V for us)
                                            the comparator D will trigger. in our case it's 2/3.
                                            see doc slau272c chap 17 for more information
                                            */



#endif
