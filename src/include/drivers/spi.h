/**
 *  \file   spi.h
 *  \brief  TI FR5739 board lib, spi
 *  \author Tristan Delizy
 *  \date   2015
 **/

#ifndef SPI_H
#define SPI_H


/******************************************************************/
/******************************************** MODULE DESCRIPTION **/

/* The SPI usage on the FR5739 is specific to the attached daugtherboard,
 * as no included module in the board communicate by SPI.
 * This driver must be configured after initialisation in order to be used.
 * The default configuration is for CC2500 RF chip usage.
 */

/******************************************************************/
/******************************************************* DEFINES **/

// SPI devices and associated Chip Select pins definitions
#define SPI_DEVICE_SUPPORTED        1

#define SPI_DEV_CC2500              0
#define SPI_CS0_PORT                1
#define SPI_CS0_BIT                 3

// #define SPI_DEV_X                x
// #define SPI_CS1_BIT              x
// #define SPI_CS1_PORT             x
// ...

// SPI pins and port definition
#define SPI_SCLK_PORT               2
#define SPI_SCLK_BIT                2

#define SPI_MOSI_PORT               1
#define SPI_MOSI_BIT                6

#define SPI_MISO_PORT               1
#define SPI_MISO_BIT                7

// SPI driver public defines
#define SPI_DUMMY_BYTE              0x55
#define SPI_NO_ACTIVE_SLAVE         0xCC

// SPI return and errors definition
#define ESUCCESS_SPI                0
#define EBADSLAVE_SPI               1
#define EOTHERSLAVE_SPI             2


/******************************************************************/
/********************************************************* TYPES **/

typedef struct {
    int reg_CTLW0;  // control register 0
    int reg_BRW;    // baud rate control register
} SPI_SETTINGS;


/******************************************************************/
/************************************************ DEFAULT VALUES **/

extern const struct spi_device_context_t    _drv_spi_init_val;
extern const SPI_SETTINGS                   spiSettings_default_config;


/******************************************************************/
/****************************************************** SYSCALLS **/

void syt_spi_init(void);
void syt_spi_configure(SPI_SETTINGS const *cfg);
unsigned int syt_spi_is_recieving(void);
unsigned int syt_spi_select_slave(unsigned int device);
void syt_spi_deselect_slave(void);
void syt_spi_read_byte(unsigned char* dst);
void syt_spi_read_frame(unsigned char* dst, unsigned int len);
void syt_spi_write_byte(const unsigned char* src);
void syt_spi_write_frame(const unsigned char* src, unsigned int len);
void syt_spi_transfert_byte(const unsigned char* src, unsigned char* dst);
void syt_spi_transfert_frame(const unsigned char* src, unsigned char* dst, unsigned int len);


/******************************************************************/
/***************************************************** FUNCTIONS **/

void spi_init(void);
void spi_drv_restore(int drv_handle);
void spi_drv_save(int drv_handle);
void spi_configure(SPI_SETTINGS const *cfg);
unsigned int spi_is_recieving(void);
unsigned int spi_select_slave(unsigned int device);
void spi_deselect_slave(void);
void spi_read_byte(unsigned char* dst);
void spi_read_frame(unsigned char* dst, unsigned int len);
void spi_write_byte(const unsigned char* src);
void spi_write_frame(const unsigned char* src, unsigned int len);
void spi_transfert_byte(const unsigned char* src, unsigned char* dst);
void spi_transfert_frame(const unsigned char* src, unsigned char* dst, unsigned int len);


#endif
