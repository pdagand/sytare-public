/**
 *  \file   clock.h
 *  \brief  TI FR5739 board lib, clock system
 *  \author Tristan Delizy
 *  \date   2016
 **/

#ifndef CLOCK_H
#define CLOCK_H


/******************************************************************/
/******************************************************* DEFINES **/

// Error modes
#define ESUCCESS_CLK                0
#define EBADRATE_CLK                1
#define EBADSOURCE_CLK              2
#define EBADDIV_CLK                 3

// DCO frequencies (if define > 10, we'll have DCORSEL = 1)
#define CLK_DCO_5_33_MHZ            ((unsigned long)5330000)
#define CLK_DCO_6_67_MHZ            ((unsigned long)6670000)
#define CLK_DCO_8_MHZ               ((unsigned long)8000000)
#define CLK_DCO_16_MHZ              ((unsigned long)16000000)
#define CLK_DCO_20_MHZ              ((unsigned long)20000000)
#define CLK_DCO_24_MHZ              ((unsigned long)24000000)

// VLO nominal frequency : 8.3kHz /!\ this value is higly dependent of voltage and temperature ! (like +/- 50%)
#define CLK_VLO_NOMINAL_FREQ        ((unsigned long)8300)

// Sytare clock system possible divider

typedef enum {
    CLK_DIV_1 =     1,
    CLK_DIV_2 =     2,
    CLK_DIV_4 =     4,
    CLK_DIV_8 =     8,
    CLK_DIV_16 =    16,
    CLK_DIV_32 =    32
} clock_div_t;

// Board possible clock sources
typedef enum {
    CLK_VLO,
    CLK_DCO
} clock_src_t;

/******************************************************************/
/****************************************************** SYSCALLS **/
void syt_clk_drv_init(void);

void syt_clk_delay_ms(unsigned int delay);

unsigned long syt_clk_get_dco_freq(void);
unsigned int syt_clk_set_dco_freq(unsigned long frequency);

unsigned long syt_clk_get_smclk_speed(void);
clock_src_t syt_clk_get_smclk_src(void);
clock_div_t syt_clk_get_smclk_div(void);
unsigned int syt_clk_set_smclk_div(clock_div_t divisor);
unsigned int syt_clk_set_smclk_src(clock_src_t source);

unsigned long syt_clk_get_mclk_speed(void);
clock_src_t syt_clk_get_mclk_src(void);
clock_div_t syt_clk_get_mclk_div(void);
unsigned int syt_clk_set_mclk_div(clock_div_t divisor);
unsigned int syt_clk_set_mclk_src(clock_src_t source);

unsigned long syt_clk_get_aclk_speed(void);
clock_src_t syt_clk_get_aclk_src(void);
clock_div_t syt_clk_get_aclk_div(void);
unsigned int syt_clk_set_aclk_div(clock_div_t divisor);
unsigned int syt_clk_set_aclk_src(clock_src_t source);

unsigned int syt_clk_dummy_call(unsigned int test1, unsigned int test2, unsigned int test3, unsigned int test4);



/******************************************************************/
/***************************************************** FUNCTIONS **/
void clk_drv_init(void);
void clk_drv_restore(int handle);
void clk_drv_save(int handle);

void clk_delay_ms(unsigned int delay);
void clk_delay_micro(unsigned int delay);

unsigned long clk_get_dco_freq(void);
unsigned int clk_set_dco_freq(unsigned long frequency);

unsigned long clk_get_smclk_speed(void);
clock_src_t clk_get_smclk_src(void);
clock_div_t clk_get_smclk_div(void);
unsigned int clk_set_smclk_div(clock_div_t divider);
unsigned int clk_set_smclk_src(clock_src_t source);

unsigned long clk_get_mclk_speed(void);
clock_src_t clk_get_mclk_src(void);
clock_div_t clk_get_mclk_div(void);
unsigned int clk_set_mclk_div(clock_div_t divider);
unsigned int clk_set_mclk_src(clock_src_t source);

unsigned long clk_get_aclk_speed(void);
clock_src_t clk_get_aclk_src(void);
clock_div_t clk_get_aclk_div(void);
unsigned int clk_set_aclk_div(clock_div_t divider);
unsigned int clk_set_aclk_src(clock_src_t source);

unsigned int clk_dummy_call(unsigned int test1, unsigned int test2, unsigned int test3, unsigned int test4);

#endif
